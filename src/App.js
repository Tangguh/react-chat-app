import React, { Component } from 'react';
import Login from './components/login/Login';
import MainChat from './components/chat/MainChat';
import { toast, ToastContainer } from 'react-toastify'
import { Provider } from "react-redux";
import store from "./store";
import {
    BrowserRouter as Router,
    Switch,
    Route
} from "react-router-dom";
import TestPage from './TestPage';

class App extends Component {
    showToast = (type, message) => {
        // 0 = warning, 1 = success
        switch (type) {
            case 0:
                toast.warning(message)
                break
            case 1:
                toast.success(message)
                break
            default:
                break
        }
    }
    render() {
        return (
            <Provider store={store}>
                <Router>
                    <ToastContainer
                        autoClose={2000}
                        hideProgressBar={true}
                        position={toast.POSITION.BOTTOM_RIGHT}
                    />
                    <Switch>
                        <Route path="/" exact render={props => <Login showToast={this.showToast} />} />
                        <Route path="/mainChat" render={props => <MainChat showToast={this.showToast} />} />
                        <Route path="/testPage" render={props => <TestPage showToast={this.showToast} />} />
                    </Switch>
                </Router>
            </Provider>
        );
    }
}

export default App;