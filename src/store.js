import { createStore } from "redux";
export const LOGGED_IN_USER = "LOGGED_IN_USER"
export const LOGGED_IN_USER_WITH = "LOGGED_IN_USER_WITH"
export const LOGGED_OUT = "LOGGED_OUT"

export const addUserAction = (data) => {
    return {
        type: LOGGED_IN_USER,
        data
    }
}
export const userChatWithAction = (data) => {
    return {
        type: LOGGED_IN_USER_WITH,
        data
    }
}

export const logoutAction = () => {
    return {
        type: LOGGED_OUT,
    }
}

const initialState = {
    currentUserId: null,
    currentUserAvatar: null,
    currentUserName: null,
    currentChatWith: null
}

const userReducer = (state = initialState, action) => {
    switch (action.type) {
        case LOGGED_IN_USER:
            return {
                ...action.data,
                currentChatWith: state.currentChatWith
            }
        case LOGGED_IN_USER_WITH:
            return {
                ...state,
                currentChatWith: action.data
            }
        case LOGGED_OUT:
            return initialState    
        default:
            return state;
    }
}

const store = createStore(userReducer);

export default store;
