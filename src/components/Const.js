export const AppString = {
    ID: 'id',
    PHOTO_URL: 'photoUrl',
    NICKNAME: 'nickname',
    EMAIL: 'email',
    NODE_MESSAGES: 'messages',
    NODE_USERS: 'users',
    DOC_ADDED: 'added',
}