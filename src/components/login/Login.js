import React, { Component } from 'react';
// Display
import { Row, Col, Card, Button } from 'antd';
import { GoogleOutlined } from "@ant-design/icons";
import 'antd/dist/antd.css';
import ImgLogin from '../../assets/img-login.svg'
import 'react-toastify/dist/ReactToastify.css'
import ReactLoading from 'react-loading'
import { withRouter } from "react-router-dom";
import './Login.css';
// Firebase
import firebase from "firebase";
import { myFirebase, myFirestore } from "../../config/MyFirebase";
import { AppString } from "../Const";
// Redux
// import store, { addUserAction } from "../../store";
// import { connect } from "react-redux";

class Login extends Component {

  constructor(props) {
    super(props)
    this.provider = new firebase.auth.GoogleAuthProvider()

    // console.log(store.getState());
    // store.dispatch(addUserAction({
    //   isLogin: true,
    //   id: '1',
    //   username: 'Tangguh',
    //   photoUrl: 'Bambans',
    //   email: 'tangguhmyname'
    // }))
    // console.log(store.getState());

    this.state = {
      isLoading: true
    }
  }

  componentDidMount() {
    this.checkLogin()

    // store.subscribe(() => {
    //   console.log('state change');
      // console.log('ini dari did mount');
      // console.log(store.getState());
    // })
  }

  checkLogin = () => {
    if (localStorage.getItem(AppString.ID)) {
      this.props.showToast(1, 'Login success')
      this.props.history.push('/mainChat')
    }
    this.setState({ isLoading: false })
  }

  onLoginPress = () => {
    // store.dispatch(addUserAction({
    //   isLogin: true,
    //   id: '1',
    //   username: 'Tangguh',
    //   photoUrl: 'Bambans',
    //   email: 'tangguhmyname'
    // }))
    this.setState({ isLoading: true })
    myFirebase
      .auth()
      .signInWithPopup(this.provider)
      .then(async result => {
        let user = result.user
        if (user) {
          const resultGet = await myFirestore
            .collection(AppString.NODE_USERS)
            .where(AppString.ID, '==', user.uid)
            .get()
          if (resultGet.docs.length === 0) {
            //Set new data for new user
            myFirestore
              .collection('users')
              .doc(user.uid)
              .set({
                id: user.uid,
                nickname: user.displayName,
                photoUrl: user.photoURL,
                email: user.email
              })
              .then(() => {
                localStorage.setItem(AppString.ID, user.uid)
                localStorage.setItem(AppString.NICKNAME, user.displayName)
                localStorage.setItem(AppString.PHOTO_URL, user.photoURL)
                localStorage.setItem(AppString.EMAIL, user.email)
                this.setState({ isLoading: false }, () => {
                  this.props.showToast(1, 'Login success')
                  this.props.history.push('/mainChat')
                })
              })
          } else {
            //Write user info to local
            localStorage.setItem(AppString.ID, resultGet.docs[0].data().id)
            localStorage.setItem(
              AppString.NICKNAME,
              resultGet.docs[0].data().nickname
            )
            localStorage.setItem(
              AppString.PHOTO_URL,
              resultGet.docs[0].data().photoUrl
            )
            localStorage.setItem(
              AppString.EMAIL,
              resultGet.docs[0].data().email
            )
            this.setState({ isLoading: false }, () => {
              this.props.showToast(1, 'Login success')
              this.props.history.push('/mainChat')
            })
          }
        } else {
          this.props.showToast(0, 'User info not available')
        }
      }).catch(err => {
        this.props.showToast(0, err.message)
        this.setState({ isLoading: false })
      })
  }

  render() {
    return (
      <div className='container-login'>
        <Row justify="space-around" align="middle">
          <Col xs={24} md={12}>
            <img src={ImgLogin} className='img-login' alt='' />
          </Col>
          <Col xs={24} md={12}>
            <div className="site-card-border-less-wrapper">
              <Card title="Login With" className="card-shadow" bordered={false} style={{ width: 300 }}>
                <Button onClick={this.onLoginPress} className="btn-gmail" shape="round" icon={<GoogleOutlined />} size="large">Login with Google</Button>
              </Card>
            </div>
          </Col>
        </Row>
        {this.state.isLoading ? (
          <div className="viewLoading">
            <ReactLoading
              type={'spin'}
              color={'#203152'}
              height={'3%'}
              width={'3%'}
            />
          </div>
        ) : null}
      </div>
    );
  }
}


export default withRouter(Login);
