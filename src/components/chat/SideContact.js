import React, { Component } from 'react';
import { Layout, Button, List, Avatar } from 'antd';
import { ArrowLeftOutlined } from '@ant-design/icons';
import { withRouter } from "react-router-dom";

const { Sider } = Layout;
// const dataContact = [
//     {
//         title: 'Joni Kopi',
//         ava: 'https://picsum.photos/200',
//         desc: 'Kopi enak',
//     },
//     {
//         title: 'Adi ABENG',
//         ava: 'https://picsum.photos/200',
//         desc: 'Anak bengkel aktip',
//     },
//     {
//         title: 'Mamang Racing',
//         ava: 'https://picsum.photos/200',
//         desc: 'Pembalap mantul',
//     },
//     {
//         title: 'Joshua GCP',
//         ava: 'https://picsum.photos/200',
//         desc: 'Anak infra',
//     },
// ];
class SideContact extends Component {
    render() {
        const dataContact = this.props.data;
        return (
            <Sider
                breakpoint="lg"
                collapsedWidth="0"
                width={300}
                className='site-layout-background'
                style={{ borderRight: '1px solid black' }}
            >
                <Button onClick={() => this.props.doLogout()} type='text' style={{ fontSize: '18px' }} icon={<ArrowLeftOutlined />} size='large'>Logout</Button>
                <List
                    className="contact-list"
                    itemLayout="horizontal"
                    dataSource={dataContact}
                    renderItem={item => (
                        <List.Item>
                            <List.Item.Meta
                                avatar={<Avatar src={item.photoUrl} size={54} />}
                                title={<button onClick={()=>this.props.userClicked(item)} href="https://ant.design">{item.nickname}</button>}
                                description={item.email}
                                
                            />
                        </List.Item>
                    )}
                />
            </Sider>
        );
    }
}

export default withRouter(SideContact);