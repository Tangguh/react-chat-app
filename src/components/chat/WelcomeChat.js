import React, { Component } from 'react';
import { Layout, Avatar, Row, Col, Typography } from 'antd';
import { withRouter } from "react-router-dom";
// Redux
import { connect } from "react-redux";
const { Content } = Layout;
const { Title } = Typography;

class WelcomeChat extends Component {
    render() {
        return (
            <Content style={{ padding: '0 24px', minHeight: 550, width: 'auto' }}>
                <Row align="middle" style={{ height: '100%' }}>
                    <Col style={{ textAlign: 'center' }} span={12} offset={6}>
                        <Avatar style={{ margin: '0.5em' }} src={this.props.ava} size={86} />
                        <Title level={3}>Welcome {this.props.nickname}</Title>
                        <p>Start your chat with us</p>
                    </Col>
                </Row>
            </Content>
        );
    }
}

const mapStateToProps = state => ({
    nickname: state.currentUserName,
    ava: state.currentUserAvatar
})

export default withRouter(connect(mapStateToProps,null)(WelcomeChat));