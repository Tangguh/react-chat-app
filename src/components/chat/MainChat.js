import React, { Component } from 'react';
import SideContact from "./SideContact";
import "./MainChat.css";
import { withRouter } from 'react-router-dom'
import ChatBoard from './ChatBoard';
import ReactLoading from 'react-loading'
import { Layout } from 'antd';
import { AppString } from "../Const";
// Firebase
import { myFirebase, myFirestore } from "../../config/MyFirebase";
import WelcomeChat from './WelcomeChat';
// Redux
import { connect } from "react-redux";
import { addUserAction, userChatWithAction } from "../../store";
class MainChat extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
        }
        this.currentUserId = localStorage.getItem(AppString.ID)
        this.currentUserAvatar = localStorage.getItem(AppString.PHOTO_URL)
        this.currentUserName = localStorage.getItem(AppString.NICKNAME)
        this.listUsers = []
    }

    checkLogin = () => {
        if (!localStorage.getItem(AppString.ID)) {
            this.setState({ isLoading: false }, () => {
                this.props.history.push('/')
            })
        } else {
            this.getListUser()
        }
    }

    componentDidMount() {
        this.checkLogin()
        this.props.logged_user({ 
            currentUserName: this.currentUserName, 
            currentUserAvatar: this.currentUserAvatar, 
            currentUserId: this.currentUserId })
    }

    getListUser = async () => {
        const result = await myFirestore.collection(AppString.NODE_USERS).get()
        if (result.docs.length > 0) {
            result.docs.forEach((item) => {
                if (item.data().id !== this.currentUserId) {
                    this.listUsers.push(
                        {
                            id: item.data().id,
                            nickname: item.data().nickname,
                            photoUrl: item.data().photoUrl,
                            email: item.data().email
                        }
                    )
                }
            })
            // console.log(this.listUsers);
            // this.listUsers = [...result.docs]
            // console.log(this.listUsers[1].data().nickname);
            this.setState({ isLoading: false })
        }
    }

    doLogout = () => {
        this.setState({ isLoading: true })
        myFirebase
            .auth()
            .signOut()
            .then(() => {
                this.setState({ isLoading: false }, () => {
                    localStorage.clear()
                    this.props.showToast(1, 'Logout success')
                    this.props.history.push('/')
                })
            })
            .catch(function (err) {
                this.setState({ isLoading: false })
                this.props.showToast(0, err.message)
            })
    }

    userClicked = (data) => {
        this.props.user_chat_with(data)
    }
    render() {
        return (
            <div className="container" style={{ border: '1px dashed #3e3e3e' }}>
                <Layout>
                    <SideContact doLogout={this.doLogout} userClicked={this.userClicked} data={this.listUsers} />
                    {this.props.currentChatWith ?
                        (
                            <ChatBoard currentChatWith={this.props.currentChatWith} showToast={this.props.showToast} />
                        ) :
                        (
                            <WelcomeChat/>
                        )
                    }

                </Layout>
                {this.state.isLoading ? (
                    <div className="viewLoading">
                        <ReactLoading
                            type={'spin'}
                            color={'#203152'}
                            height={'3%'}
                            width={'3%'}
                        />
                    </div>
                ) : null}
            </div>
        );
    }
}

const mapDispatchToProps = (dispatch) => ({
    logged_user: data => dispatch(addUserAction(data)),
    user_chat_with: data => dispatch(userChatWithAction(data))
})

const mapStateToProps = (state) => ({
    currentChatWith: state.currentChatWith
})


export default withRouter(connect(mapStateToProps, mapDispatchToProps)(MainChat));

