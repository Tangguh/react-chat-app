import React, { Component } from 'react';
import ReactLoading from 'react-loading'
import 'react-toastify/dist/ReactToastify.css'
import { Layout, Button, Avatar, Row, Col, Typography } from 'antd';
import { SendOutlined } from '@ant-design/icons';
import moment from 'moment'
// firebase
import { myFirestore } from "../../config/MyFirebase";
import { AppString } from "../Const"
// Redux
import { connect } from "react-redux";

const { Title } = Typography;
const { Content } = Layout;

class ChatBoard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            inputValue: ''
        }
        // this.props.data.currentUserId = localStorage.getItem(AppString.ID)
        // this.props.data.currentUserAvatar = localStorage.getItem(AppString.PHOTO_URL)
        // this.currentUserName = localStorage.getItem(AppString.NICKNAME)
        this.listMessage = []
        this.currentChatWith = this.props.currentChatWith
        this.groupChatId = null
        this.removeListener = null
    }

    componentDidUpdate() {
        this.scrollToBottom()
    }

    componentDidMount() {
        this.getListHistory()
    }

    componentWillUnmount() {
        this.removeListener()
    }

    UNSAFE_componentWillReceiveProps(newProps) {
        if (newProps.currentChatWith) {
            this.currentChatWith = newProps.currentChatWith
            this.getListHistory()
        }
    }

    getListHistory = () => {
        if (this.removeListener) {
            this.removeListener()
        }
        this.listMessage.length = 0
        this.setState({ isLoading: true })
        if (this.hashString(this.props.data.currentUserId) <= this.hashString(this.currentChatWith.id)) {
            this.groupChatId = `${this.props.data.currentUserId}-${this.currentChatWith.id}`
        } else {
            this.groupChatId = `${this.currentChatWith.id}-${this.props.data.currentUserId}`
        }

        this.removeListener = myFirestore
            .collection(AppString.NODE_MESSAGES)
            .doc(this.groupChatId)
            .collection(this.groupChatId)
            .onSnapshot(
                snapshot => {
                    snapshot.docChanges().forEach(change => {
                        if (change.type === AppString.DOC_ADDED) {
                            this.listMessage.push(change.doc.data())
                        }
                    })
                    this.setState({ isLoading: false })
                }, err => {
                    this.props.showToast(0, err.toString())
                }
            )
    }

    hashString = str => {
        let hash = 0
        for (let i = 0; i < str.length; i++) {
            hash += Math.pow(str.charCodeAt(i) * 31, str.length - i)
            hash = hash & hash
        }
        return hash
    }
    scrollToBottom = () => {
        if (this.messagesEnd) {
            this.messagesEnd.scrollIntoView({})
        }
    }

    onKeyboardPress = event => {
        if (event.key === 'Enter') {
            this.onSendMessage(this.state.inputValue, 0)
        }
    }

    onSendMessage = (content, type) => {
        if (content.trim() === '') {
            return
        }

        const timestamp = moment().valueOf().toString()


        const itemMessage = {
            idFrom: this.props.data.currentUserId,
            idTo: this.currentChatWith.id,
            timestamp: timestamp,
            content: content.trim(),
            type: type
        }
        console.log(this.groupChatId);
        console.log(itemMessage);
        myFirestore
            .collection(AppString.NODE_MESSAGES)
            .doc(this.groupChatId)
            .collection(this.groupChatId)
            .doc(timestamp)
            .set(itemMessage)
            .then(() => {
                this.setState({ inputValue: '' })
            })
            .catch(err => {
                this.props.showToast(0, err.toString())
            })
    }

    renderListMessages = () => {
        if (this.listMessage.length > 0) {
            let viewListMessage = []
            this.listMessage.forEach((item, index) => {
                if (item.idFrom === this.props.data.currentUserId) {
                    if (item.type === 0) {
                        viewListMessage.push(
                            <div className="chat self" key={item.timestamp}>
                                <Avatar className='userphoto' src={this.props.data.currentUserAvatar} />
                                <p className="chatmessage">{item.content}</p>
                            </div>
                        )
                    } else {
                        viewListMessage.push(
                            <div className="chat self" key={index}>
                                <p className="chatmessage">Message unavailable</p>
                            </div>
                        )
                    }
                } else {
                    if (item.type === 0) {
                        viewListMessage.push(
                            <div className="chat friend" key={item.timestamp}>
                                <Avatar className='userphoto' src={this.currentChatWith.photoUrl} />
                                <p className="chatmessage">{item.content}</p>
                            </div>
                        )
                    } else {
                        viewListMessage.push(
                            <div className="chat friend" key={index}>
                                <p className="chatmessage">Message unavailable</p>
                            </div>
                        )
                    }
                }
            })
            return viewListMessage
        } else {
            return (
                <Row align="middle" style={{ height: '100%' }}>
                    <Col style={{ textAlign: 'center' }} span={12} offset={6}>
                        <Title level={3}>Say Hi to your friend <span role='img' aria-label='emoji'>🤙🖐</span></Title>
                    </Col>
                </Row>
            )
        }
    }

    render() {
        return (
            <Content className='chatbox' style={{ padding: '0 10px', minHeight: 280 }}>
                <div className='peerChat'>
                    <Avatar className='ava' src={this.currentChatWith.photoUrl} size={44} />
                    <p className='username'>{this.currentChatWith.nickname}</p>
                </div>
                <div className='chatlogs'>
                    {this.renderListMessages()}
                </div>
                <div className="chat-form">
                    <textarea
                        placeholder="Type your message..."
                        value={this.state.inputValue}
                        onChange={e => {
                            this.setState({ inputValue: e.target.value })
                        }}
                        onKeyPress={this.onKeyboardPress}
                    ></textarea>
                    <Button type='primary' onClick={() => this.onSendMessage(this.state.inputValue, 0)} style={{ backgroundColor: '#1adda4' }} shape="round" icon={<SendOutlined />} size='large' />
                </div>
                {this.state.isLoading ? (
                    <div className="viewLoading">
                        <ReactLoading
                            type={'spin'}
                            color={'#203152'}
                            height={'3%'}
                            width={'3%'}
                        />
                    </div>
                ) : null}
            </Content>
        );
    }
}

const mapStateToProps = (state) => ({
    data: state
})

export default connect(mapStateToProps)(ChatBoard);