import React, { Component } from 'react';
// Redux
import store from "./store";
import { connect } from "react-redux";

class TestPage extends Component {
    constructor() {
        super();
        this.state = {
            isLogin: false,
            id: '',
            username: '',
            photoUrl: '',
            email: ''
        }
        console.log(store.getState());
    }

    componentDidMount() {
        const user = store.getState()
        this.setState({
            isLogin: user.isLogin,
            id: user.id,
            username: user.username,
            photoUrl: user.photoUrl,
            email: user.email
        })
        // store.subscribe(() => {
        //     console.log('state change');
        //     console.log(store.getState());
        //   })
       
    }

    render() {
        return (
            <div>
                {this.state.isLogin.toString()}
            </div>
        );
    }
}

const mapStateToProps = state => {
    console.log('ini dari mstp');
    console.log(state);
}

export default connect(mapStateToProps)(TestPage);